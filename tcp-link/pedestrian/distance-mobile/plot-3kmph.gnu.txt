#======== TCP-UDP-Delay ========
set title "3kmph trace: put vs distance (uplink, 100RBs)"
set key inside bottom right box
set xlabel "Distance from eNB (m)"
set xtic 100
set ylabel "TCP/UDP throughput (kbps)"
#set y2label "Uplink TCP delay (ms)
set output "tcp-udp-delay-3kmph.svg"
#set log y
#set autoscale y
#set autoscale y2
#set yrange [0:30000]
#set y2range [0:30000]
set y2tics nomirror tc lt  3
#set y2tic 10


set terminal svg

plot "3kmph" using 1:3 title "TCP Throughput" with line ,\
"3kmph" using 1:15 title "UDP throughput" with line,\
"3kmph" using 1:9 title "Uplink TCP delay" with line axes x2y2
