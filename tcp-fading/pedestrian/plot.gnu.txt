set title "TCP performance with pedestrian fading (UMRLC downlink)"
set key inside bottom right box
set xlabel "Time (s)"
set xtic 5
set ylabel "TCP goodput (kbps)"
set y2label "TCP delay (ms)"
set output "tcp-put-pedestrian.svg"
#set log y
set autoscale y
set autoscale y2
set y2tics nomirror tc lt  1
set y2tic 10


set terminal svg

plot "tcp-put-pedestrian.txt" using ($1/1000):3 title "Tcp Dl Delay" with line axes x2y2,\
"tcp-put-pedestrian.txt" using ($1/1000):2 title "TCP throughput" with line,\
"link_cap_dl.txt" using ($1/1000):2 title "Downlink capacity (Mbps)" axes x2y2,\
"link_cap_ul.txt" using ($1/1000):2 title "Uplink capacity (Mbps)" axes x2y2
