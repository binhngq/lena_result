set title "TCP application Dl Delay (UMRLC, 16Mbps radio link)"
set key inside bottom right box
set xlabel "TCP Rate (Mbps)"
set xtic 5
set ylabel "Delay (ms)"
set y2label "Goodput (Mbps)"
set output "tcp-dl.svg"
#set log y
set autoscale y
set autoscale y2
set y2range [0:20]
set y2tics nomirror tc lt  1
set y2tic 5


set terminal svg

plot "output-tcp-dl.txt" using ($2/1024):12 title "Goodput" with line axes x2y2, \
"output-tcp-dl.txt" using ($2/1024):13 title "Tcp Dl Delay" with line,\
"output-tcp-dl.txt" using ($2/1024):14 title "DL PDCP PDUs delay" with line,\
"output-tcp-dl.txt" using ($2/1024):15 title "DL RLC PDUs delay" with line,\
"output-tcp-dl.txt" using ($2/1024):3 title "Mean AckTx Rate" with line axes x2y2
